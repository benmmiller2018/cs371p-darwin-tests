*** Darwin 9x9 ***
Turn = 0.
  012345678
0 .f...h...
1 .........
2 ........r
3 .h.......
4 ........t
5 .......f.
6 ........t
7 .........
8 ......r..

Turn = 2.
  012345678
0 .f...h...
1 .........
2 .........
3 ...h....r
4 ........r
5 .......f.
6 ......r.t
7 .........
8 .........

Turn = 4.
  012345678
0 .f...h...
1 .........
2 .........
3 .....h..r
4 ......r..
5 .......ft
6 ........t
7 .........
8 .........

Turn = 6.
  012345678
0 .f...h...
1 .........
2 .........
3 ......rr.
4 ........r
5 .......fr
6 ........t
7 .........
8 .........

Turn = 8.
  012345678
0 .f...h...
1 ......r..
2 .........
3 ........r
4 .......r.
5 .......fr
6 ........r
7 .........
8 .........

*** Darwin 8x4 ***
Turn = 0.
  0123
0 ....
1 ....
2 ..f.
3 ....
4 .h.h
5 .h.h
6 ....
7 ....

Turn = 5.
  0123
0 .h..
1 ....
2 ..f.
3 ....
4 h..h
5 ...h
6 ....
7 ....

Turn = 10.
  0123
0 .h..
1 ....
2 ..f.
3 ....
4 h..h
5 ...h
6 ....
7 ....

Turn = 15.
  0123
0 .h..
1 ....
2 ..f.
3 ....
4 h..h
5 ...h
6 ....
7 ....

Turn = 20.
  0123
0 .h..
1 ....
2 ..f.
3 ....
4 h..h
5 ...h
6 ....
7 ....

Turn = 25.
  0123
0 .h..
1 ....
2 ..f.
3 ....
4 h..h
5 ...h
6 ....
7 ....

*** Darwin 9x10 ***
Turn = 0.
  0123456789
0 ..h.......
1 ..........
2 .h.....f.h
3 ..........
4 ..........
5 .........t
6 .t....t...
7 ..........
8 .r.......t

Turn = 5.
  0123456789
0 h.........
1 ..........
2 ......hf..
3 ..........
4 .........t
5 r........t
6 .t....t...
7 ..........
8 .........t

Turn = 10.
  0123456789
0 r.........
1 r.........
2 ......hf..
3 ..........
4 .........t
5 .........t
6 .t....t...
7 ..........
8 .........t

Turn = 15.
  0123456789
0 .r........
1 ....r.....
2 ......hf..
3 ..........
4 .........t
5 .........t
6 .t....t...
7 ..........
8 .........t

Turn = 20.
  0123456789
0 ......r...
1 .........r
2 ......hf..
3 ..........
4 .........t
5 .........t
6 .t....t...
7 ..........
8 .........t

Turn = 25.
  0123456789
0 ........rr
1 ..........
2 ......hf..
3 ..........
4 .........t
5 .........t
6 .t....t...
7 ..........
8 .........t

Turn = 30.
  0123456789
0 ....r.....
1 ..........
2 ......hf..
3 ..........
4 .........t
5 ........rt
6 .t....t...
7 ..........
8 .........t

Turn = 35.
  0123456789
0 r.........
1 ..........
2 ......hf..
3 ..........
4 .........t
5 .........t
6 .t....t...
7 ..........
8 .......r.t

Turn = 40.
  0123456789
0 ..........
1 ..........
2 ......hf..
3 r.........
4 .........t
5 .........t
6 .t....t...
7 ..........
8 ..r......t

Turn = 45.
  0123456789
0 ..........
1 ..........
2 ......hf..
3 ..........
4 .........t
5 .........t
6 .t....t...
7 r.........
8 .r.......t

*** Darwin 4x3 ***
Turn = 0.
  012
0 .r.
1 ff.
2 f.t
3 .th

Turn = 3.
  012
0 ..r
1 ff.
2 f.t
3 .tt

Turn = 6.
  012
0 ...
1 ffr
2 f.t
3 .tt

Turn = 9.
  012
0 ...
1 frr
2 f.r
3 .tr

Turn = 12.
  012
0 r..
1 rr.
2 f.t
3 .tt

*** Darwin 7x10 ***
Turn = 0.
  0123456789
0 ..........
1 ..........
2 ..........
3 .f....r...
4 ..f.......
5 ..........
6 ..........

Turn = 4.
  0123456789
0 ..........
1 ..........
2 ..........
3 .fr.......
4 ..f.......
5 ..........
6 ..........

Turn = 8.
  0123456789
0 ..........
1 ..........
2 ..........
3 ..........
4 ..r.r.....
5 .r........
6 ..........

Turn = 12.
  0123456789
0 ..........
1 ..........
2 ..........
3 ..........
4 ........r.
5 ..........
6 rr........

Turn = 16.
  0123456789
0 ..........
1 ..........
2 r.........
3 ..........
4 ..........
5 ..........
6 .r.......r

Turn = 20.
  0123456789
0 .r........
1 ..........
2 ..........
3 ..........
4 ..........
5 ..........
6 .....rr...

Turn = 24.
  0123456789
0 .....r....
1 ..........
2 ..........
3 .....rr...
4 ..........
5 ..........
6 ..........

Turn = 28.
  0123456789
0 .....rr..r
1 ..........
2 ..........
3 ..........
4 ..........
5 ..........
6 ..........

Turn = 32.
  0123456789
0 .r.......r
1 ..........
2 ..........
3 .........r
4 ..........
5 ..........
6 ..........

*** Darwin 5x3 ***
Turn = 0.
  012
0 ..f
1 ...
2 rh.
3 ..r
4 .fh

Turn = 5.
  012
0 ..f
1 ..r
2 ...
3 rr.
4 .rr

Turn = 10.
  012
0 r.r
1 r..
2 .r.
3 ...
4 .rr

Turn = 15.
  012
0 .rr
1 r.r
2 ..r
3 ...
4 r..

Turn = 20.
  012
0 ..r
1 rr.
2 ..r
3 ...
4 .rr

Turn = 25.
  012
0 rr.
1 ...
2 ..r
3 .r.
4 r.r

Turn = 30.
  012
0 ...
1 r..
2 .r.
3 rr.
4 r.r

Turn = 35.
  012
0 rr.
1 .r.
2 r..
3 r.r
4 ...

Turn = 40.
  012
0 rr.
1 ...
2 ..r
3 .rr
4 r..

*** Darwin 6x4 ***
Turn = 0.
  0123
0 ..h.
1 .h.h
2 ..f.
3 ft..
4 .f..
5 .th.

Turn = 4.
  0123
0 ...h
1 h.h.
2 ..f.
3 tth.
4 .t..
5 .t..

Turn = 8.
  0123
0 ...h
1 h.t.
2 ..t.
3 ttt.
4 .t..
5 .t..

Turn = 12.
  0123
0 ...h
1 h.t.
2 ..t.
3 ttt.
4 .t..
5 .t..

Turn = 16.
  0123
0 ...h
1 h.t.
2 ..t.
3 ttt.
4 .t..
5 .t..

Turn = 20.
  0123
0 ...h
1 h.t.
2 ..t.
3 ttt.
4 .t..
5 .t..

*** Darwin 3x6 ***
Turn = 0.
  012345
0 h...f.
1 .rr...
2 ..f.tt

Turn = 5.
  012345
0 ....f.
1 rr....
2 r.f.tt

Turn = 10.
  012345
0 r.rrr.
1 ......
2 .r..tt

Turn = 15.
  012345
0 r..r..
1 r..r..
2 .r..rr

*** Darwin 8x7 ***
Turn = 0.
  0123456
0 r...t..
1 .rh....
2 ..r....
3 .......
4 ....hr.
5 .....f.
6 ......h
7 .......

Turn = 2.
  0123456
0 ....t..
1 .......
2 r.h....
3 .r.....
4 ..r....
5 .....r.
6 ....hh.
7 .....r.

Turn = 4.
  0123456
0 ....t..
1 .......
2 .......
3 .......
4 r.h....
5 .r.....
6 ..rr.r.
7 ....rr.

Turn = 6.
  0123456
0 ....t..
1 .......
2 .......
3 .......
4 .......
5 ..h....
6 rr...rr
7 .rrr...

Turn = 8.
  0123456
0 ....t..
1 .......
2 .......
3 .......
4 .....r.
5 .......
6 r.hr...
7 rrr...r

Turn = 10.
  0123456
0 ....t..
1 .......
2 .....r.
3 .......
4 r..r...
5 .......
6 .......
7 rrh.r.r

*** Darwin 4x8 ***
Turn = 0.
  01234567
0 .....t.f
1 .r.....h
2 ........
3 ....r...

Turn = 2.
  01234567
0 .....t.f
1 r......h
2 ........
3 ......r.

Turn = 4.
  01234567
0 r....t.f
1 .......h
2 ........
3 .......r

Turn = 6.
  01234567
0 r....t.f
1 .......h
2 ........
3 ......r.

Turn = 8.
  01234567
0 ..r..t.f
1 .......h
2 ........
3 ....r...

Turn = 10.
  01234567
0 ....rt.f
1 .......h
2 ........
3 ..r.....

Turn = 12.
  01234567
0 ....r..f
1 .....r.h
2 ........
3 r.......

Turn = 14.
  01234567
0 .....r.f
1 .......h
2 r.......
3 .....r..

Turn = 16.
  01234567
0 r.....rr
1 .......h
2 ........
3 ......r.

Turn = 18.
  01234567
0 r......r
1 ......rr
2 ........
3 .......r

Turn = 20.
  01234567
0 .......r
1 .....r.r
2 r.......
3 ......r.

Turn = 22.
  01234567
0 .......r
1 ...r..r.
2 ........
3 r....r..

Turn = 24.
  01234567
0 ........
1 .r..r...
2 .......r
3 ..rr....

Turn = 26.
  01234567
0 ........
1 r.r.....
2 ..r.....
3 ...r...r

Turn = 28.
  01234567
0 r.r.....
1 r.......
2 ........
3 .r...r..

Turn = 30.
  01234567
0 .rr.....
1 ........
2 r.......
3 r..r....

*** Darwin 9x1 ***
Turn = 0.
  0
0 f
1 .
2 r
3 .
4 t
5 f
6 .
7 .
8 .

Turn = 2.
  0
0 f
1 r
2 .
3 .
4 t
5 f
6 .
7 .
8 .

Turn = 4.
  0
0 r
1 r
2 .
3 .
4 t
5 t
6 .
7 .
8 .

Turn = 6.
  0
0 r
1 .
2 r
3 .
4 t
5 t
6 .
7 .
8 .

Turn = 8.
  0
0 r
1 .
2 .
3 r
4 r
5 .
6 r
7 .
8 .

Turn = 10.
  0
0 r
1 .
2 .
3 r
4 .
5 .
6 r
7 .
8 r

Turn = 12.
  0
0 .
1 r
2 r
3 .
4 .
5 .
6 .
7 r
8 r

Turn = 14.
  0
0 .
1 r
2 r
3 .
4 .
5 .
6 r
7 .
8 r

Turn = 16.
  0
0 r
1 r
2 .
3 .
4 r
5 .
6 r
7 .
8 .

Turn = 18.
  0
0 r
1 r
2 r
3 .
4 r
5 .
6 .
7 .
8 .

Turn = 20.
  0
0 r
1 r
2 r
3 r
4 .
5 .
6 .
7 .
8 .

Turn = 22.
  0
0 r
1 r
2 r
3 .
4 r
5 .
6 .
7 .
8 .

Turn = 24.
  0
0 r
1 r
2 r
3 .
4 .
5 .
6 r
7 .
8 .

*** Darwin 5x9 ***
Turn = 0.
  012345678
0 .......r.
1 ..r......
2 .....r...
3 .........
4 tt.......

Turn = 2.
  012345678
0 ..r..r..r
1 .........
2 .........
3 .........
4 tt.......

Turn = 4.
  012345678
0 r.....r..
1 ........r
2 .........
3 .........
4 tt.......

Turn = 6.
  012345678
0 r.......r
1 .........
2 .........
3 ........r
4 tt.......

Turn = 8.
  012345678
0 ..r.....r
1 .........
2 .........
3 .........
4 tt......r

Turn = 10.
  012345678
0 ....r.r..
1 .........
2 .........
3 .........
4 tt....r..

Turn = 12.
  012345678
0 .....rr..
1 .........
2 .........
3 .........
4 tt..r....

Turn = 14.
  012345678
0 ....r...r
1 .........
2 .........
3 .........
4 ttr......

Turn = 16.
  012345678
0 ..r......
1 ........r
2 .........
3 .........
4 rrr......

Turn = 18.
  012345678
0 r........
1 .........
2 ..r......
3 ........r
4 rr.......

Turn = 20.
  012345678
0 ..r......
1 r........
2 .........
3 .........
4 r..r....r

Turn = 22.
  012345678
0 ...r.....
1 .........
2 .........
3 r........
4 ..r..rr..

Turn = 24.
  012345678
0 .....r...
1 .........
2 .........
3 .....r...
4 r...r.r..

Turn = 26.
  012345678
0 .......r.
1 .....r...
2 .........
3 ......r..
4 ..r..r...

Turn = 28.
  012345678
0 .....r..r
1 ......r..
2 .........
3 .........
4 ....rr...

Turn = 30.
  012345678
0 ...r..r.r
1 .........
2 .........
3 .........
4 ....rr...

Turn = 32.
  012345678
0 .r..r....
1 .........
2 .....r..r
3 .........
4 ....r....

Turn = 34.
  012345678
0 r.r..r...
1 .........
2 .........
3 .........
4 ..r.....r

Turn = 36.
  012345678
0 rr....r..
1 .........
2 .........
3 .........
4 r.......r

Turn = 38.
  012345678
0 ..r.....r
1 .........
2 r.......r
3 .........
4 r........

Turn = 40.
  012345678
0 ....r...r
1 ........r
2 .........
3 r........
4 r........

Turn = 42.
  012345678
0 ......r..
1 ......r.r
2 .........
3 ..r......
4 ..r......

Turn = 44.
  012345678
0 ........r
1 ....r....
2 .........
3 ....r...r
4 ....r....

*** Darwin 9x5 ***
Turn = 0.
  01234
0 .....
1 ...f.
2 .....
3 .....
4 .f.fh
5 ..f.r
6 ..r..
7 ....r
8 .....

Turn = 1.
  01234
0 .....
1 ...f.
2 .....
3 .....
4 .f.fr
5 ..f.r
6 ...r.
7 ....r
8 .....

Turn = 2.
  01234
0 .....
1 ...f.
2 .....
3 .....
4 .f.fr
5 ..f.r
6 ....r
7 .....
8 ....r

Turn = 3.
  01234
0 .....
1 ...f.
2 .....
3 ....r
4 .f.f.
5 ..f.r
6 ....r
7 .....
8 ....r

*** Darwin 3x4 ***
Turn = 0.
  0123
0 tt.f
1 ....
2 f..h

Turn = 3.
  0123
0 tt.f
1 ....
2 fh..

Turn = 6.
  0123
0 tt.f
1 ....
2 fh..

Turn = 9.
  0123
0 tt.f
1 ....
2 fh..

Turn = 12.
  0123
0 tt.f
1 ....
2 fh..

Turn = 15.
  0123
0 tt.f
1 ....
2 fh..

Turn = 18.
  0123
0 tt.f
1 ....
2 fh..

Turn = 21.
  0123
0 tt.f
1 ....
2 fh..

Turn = 24.
  0123
0 tt.f
1 ....
2 fh..

Turn = 27.
  0123
0 tt.f
1 ....
2 fh..

Turn = 30.
  0123
0 tt.f
1 ....
2 fh..

Turn = 33.
  0123
0 tt.f
1 ....
2 fh..

*** Darwin 6x6 ***
Turn = 0.
  012345
0 .f..r.
1 ...h..
2 ..r...
3 r.....
4 ht....
5 f.....

Turn = 1.
  012345
0 .f....
1 ....r.
2 ..rr..
3 r.....
4 rt....
5 f.....

Turn = 2.
  012345
0 .f....
1 ......
2 ..r.r.
3 r..r..
4 rr....
5 f.....

Turn = 3.
  012345
0 .f....
1 ..r...
2 ......
3 rr..r.
4 r..r..
5 f.....

Turn = 4.
  012345
0 .fr...
1 ......
2 rr....
3 ......
4 r...r.
5 .r.r..

Turn = 5.
  012345
0 .fr...
1 rr....
2 ......
3 ......
4 ......
5 r.rrr.

Turn = 6.
  012345
0 rrr...
1 .r....
2 ......
3 ......
4 ......
5 r.rrr.

Turn = 7.
  012345
0 rrr...
1 ..r...
2 ......
3 ......
4 ..r...
5 r..r.r

Turn = 8.
  012345
0 r.r...
1 .r.r..
2 ......
3 ..r...
4 ......
5 r...rr

Turn = 9.
  012345
0 r..r..
1 ....r.
2 .rr...
3 ......
4 .....r
5 r...r.

Turn = 10.
  012345
0 .r..r.
1 ..r..r
2 ......
3 .r...r
4 r...r.
5 ......

Turn = 11.
  012345
0 ..r..r
1 ..r..r
2 .....r
3 r...r.
4 .r....
5 ......

Turn = 12.
  012345
0 ...r.r
1 .r...r
2 r...rr
3 ......
4 ......
5 .r....

Turn = 13.
  012345
0 ....rr
1 r...rr
2 r....r
3 ......
4 ......
5 .r....

Turn = 14.
  012345
0 ....rr
1 r...rr
2 r....r
3 ......
4 ......
5 r.....

Turn = 15.
  012345
0 ....rr
1 r...rr
2 r....r
3 ......
4 ......
5 r.....

Turn = 16.
  012345
0 ...r.r
1 .r...r
2 r...r.
3 .....r
4 ......
5 r.....

Turn = 17.
  012345
0 ..r..r
1 r.r..r
2 ......
3 ....r.
4 .....r
5 .r....

Turn = 18.
  012345
0 rr...r
1 ...r..
2 .....r
3 ......
4 ....r.
5 ..r..r

Turn = 19.
  012345
0 rr...r
1 ....r.
2 ......
3 .....r
4 ......
5 ...rrr

Turn = 20.
  012345
0 r....r
1 .r...r
2 ......
3 ......
4 .....r
5 ...rrr

Turn = 21.
  012345
0 r....r
1 .....r
2 .r....
3 ......
4 .....r
5 ...rrr

Turn = 22.
  012345
0 r....r
1 ......
2 .....r
3 .r....
4 ....r.
5 ...rrr

Turn = 23.
  012345
0 r....r
1 ......
2 ......
3 .....r
4 .r.r..
5 ...rrr

Turn = 24.
  012345
0 .r..r.
1 ......
2 ......
3 ......
4 ..r.rr
5 .rr..r

Turn = 25.
  012345
0 ..rr..
1 ......
2 ......
3 ....r.
4 .r...r
5 .rr..r

Turn = 26.
  012345
0 ..rr..
1 ......
2 ....r.
3 ......
4 r.r..r
5 .r...r

Turn = 27.
  012345
0 ..rr..
1 ....r.
2 ......
3 ..r..r
4 rr...r
5 ......

Turn = 28.
  012345
0 .r..r.
1 ....r.
2 ..r..r
3 .r...r
4 ......
5 r.....

Turn = 29.
  012345
0 r....r
1 ..rr.r
2 .r...r
3 ......
4 ......
5 r.....

Turn = 30.
  012345
0 r.r..r
1 .rr..r
2 .....r
3 ......
4 ......
5 .r....

*** Darwin 3x4 ***
Turn = 0.
  0123
0 t...
1 h...
2 ...t

Turn = 1.
  0123
0 t...
1 ....
2 h..t

Turn = 2.
  0123
0 t...
1 ....
2 h..t

Turn = 3.
  0123
0 t...
1 ....
2 h..t

Turn = 4.
  0123
0 t...
1 ....
2 h..t

Turn = 5.
  0123
0 t...
1 ....
2 h..t

Turn = 6.
  0123
0 t...
1 ....
2 h..t

Turn = 7.
  0123
0 t...
1 ....
2 h..t

Turn = 8.
  0123
0 t...
1 ....
2 h..t

Turn = 9.
  0123
0 t...
1 ....
2 h..t

Turn = 10.
  0123
0 t...
1 ....
2 h..t

Turn = 11.
  0123
0 t...
1 ....
2 h..t

Turn = 12.
  0123
0 t...
1 ....
2 h..t

Turn = 13.
  0123
0 t...
1 ....
2 h..t

Turn = 14.
  0123
0 t...
1 ....
2 h..t

Turn = 15.
  0123
0 t...
1 ....
2 h..t

Turn = 16.
  0123
0 t...
1 ....
2 h..t

Turn = 17.
  0123
0 t...
1 ....
2 h..t

Turn = 18.
  0123
0 t...
1 ....
2 h..t

Turn = 19.
  0123
0 t...
1 ....
2 h..t

Turn = 20.
  0123
0 t...
1 ....
2 h..t

Turn = 21.
  0123
0 t...
1 ....
2 h..t

*** Darwin 1x5 ***
Turn = 0.
  01234
0 .ft..

Turn = 2.
  01234
0 .tt..

Turn = 4.
  01234
0 .tt..

Turn = 6.
  01234
0 .tt..

Turn = 8.
  01234
0 .tt..

Turn = 10.
  01234
0 .tt..

*** Darwin 1x3 ***
Turn = 0.
  012
0 .h.

Turn = 1.
  012
0 h..

Turn = 2.
  012
0 h..

Turn = 3.
  012
0 h..

Turn = 4.
  012
0 h..

Turn = 5.
  012
0 h..

Turn = 6.
  012
0 h..

Turn = 7.
  012
0 h..

Turn = 8.
  012
0 h..

Turn = 9.
  012
0 h..

Turn = 10.
  012
0 h..

Turn = 11.
  012
0 h..

Turn = 12.
  012
0 h..

Turn = 13.
  012
0 h..

Turn = 14.
  012
0 h..

Turn = 15.
  012
0 h..

Turn = 16.
  012
0 h..

Turn = 17.
  012
0 h..

Turn = 18.
  012
0 h..

*** Darwin 8x4 ***
Turn = 0.
  0123
0 f...
1 ....
2 ..r.
3 ...f
4 .f..
5 ....
6 .h..
7 ....

Turn = 4.
  0123
0 f...
1 r...
2 ....
3 ...f
4 .f..
5 ....
6 ...h
7 ....

Turn = 8.
  0123
0 ..r.
1 ..r.
2 ....
3 ...f
4 .f..
5 ....
6 ...h
7 ....

Turn = 12.
  0123
0 ..r.
1 ....
2 ...r
3 ...r
4 .f..
5 ....
6 ...h
7 ....

Turn = 16.
  0123
0 r...
1 ....
2 ....
3 r...
4 .f.r
5 ....
6 ...h
7 ....

Turn = 20.
  0123
0 ....
1 .r..
2 ..r.
3 ....
4 .f..
5 ...r
6 ....
7 ...r

Turn = 24.
  0123
0 ...r
1 ....
2 ..r.
3 ...r
4 .f.r
5 ....
6 ....
7 ....

Turn = 28.
  0123
0 r..r
1 ...r
2 ....
3 r...
4 .f..
5 ....
6 ....
7 ....

Turn = 32.
  0123
0 r..r
1 ...r
2 ....
3 ....
4 .f..
5 ....
6 ....
7 r...

Turn = 36.
  0123
0 ..rr
1 ....
2 ....
3 ....
4 .f.r
5 r...
6 ....
7 ....

Turn = 40.
  0123
0 ....
1 r...
2 ....
3 ..rr
4 .f..
5 ....
6 ....
7 ...r

Turn = 44.
  0123
0 ....
1 r...
2 ....
3 ....
4 .f..
5 ....
6 ....
7 r.rr

*** Darwin 4x7 ***
Turn = 0.
  0123456
0 ..h..rt
1 .......
2 ..fff..
3 .r.....

Turn = 1.
  0123456
0 ...h..t
1 .....r.
2 ..fff..
3 ..r....

Turn = 2.
  0123456
0 ....h.t
1 .......
2 ..fffr.
3 ...r...

Turn = 3.
  0123456
0 .....ht
1 .......
2 ..fff..
3 ....rr.

Turn = 4.
  0123456
0 .....tt
1 .......
2 ..fff..
3 ....rr.

Turn = 5.
  0123456
0 .....tt
1 .......
2 ..fff..
3 ....r.r
